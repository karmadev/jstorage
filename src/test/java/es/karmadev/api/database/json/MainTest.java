package es.karmadev.api.database.json;

import es.karmadev.api.database.DatabaseManager;
import es.karmadev.api.database.DatabaseProvider;
import es.karmadev.api.database.json.provider.DefaultProvider;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    @Test
    public void testThatServiceProviderWorks() {
        assertDoesNotThrow(DatabaseManager::getInstance);
    }

    @Test
    public void testThatServiceProviderWorksAndReturnsAProvider() {
        DatabaseManager manager = DatabaseManager.getInstance();
        assertDoesNotThrow(() -> manager.getProvider());
    }

    @Test
    public void testThatServiceProviderReturnsADefaultProvider() {
        DatabaseManager manager = DatabaseManager.getInstance();
        DatabaseProvider provider = manager.getProvider();

        assertInstanceOf(DefaultProvider.class, provider);
    }

    @Test
    public void testThatProviderInstanceIsAlwaysTheSame() {
        DatabaseManager manager = DatabaseManager.getInstance();
        DatabaseProvider provider1 = manager.getProvider();
        DatabaseProvider provider2 = manager.getProvider();

        assertEquals(provider1, provider2);
    }

    @Test
    public void testThatProviderInstanceIsAlwaysTheSameAfterUpdate() {
        DatabaseManager manager = DatabaseManager.getInstance();
        DatabaseProvider provider1 = manager.getProvider();
        manager = manager.update();

        DatabaseProvider provider2 = manager.getProvider();

        assertEquals(provider1, provider2);
    }
}
