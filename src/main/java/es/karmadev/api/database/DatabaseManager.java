package es.karmadev.api.database;

import java.util.*;

/**
 * Represents a database manager
 */
public class DatabaseManager {

    private final static Collection<DatabaseProvider> providers = new ArrayList<>();
    private final static DatabaseManager INSTANCE = new DatabaseManager();

    private static synchronized void load() {
        ServiceLoader<DatabaseProvider> engineLoader = ServiceLoader.load(DatabaseProvider.class);
        Iterator<DatabaseProvider> engineProviderIterator = engineLoader.iterator();
        List<DatabaseProvider> engineProviderList = new ArrayList<>();
        engineProviderIterator.forEachRemaining(engineProviderList::add);

        updateEngineProviders(engineProviderList);
    }

    private static synchronized void updateEngineProviders(final List<DatabaseProvider> engines) {
        List<DatabaseProvider> toAdd = new ArrayList<>();

        mainLoop:
        for (DatabaseProvider provider : engines) {
            if (provider == null) continue;

            for (DatabaseProvider existing : DatabaseManager.providers) {
                if (existing == null) continue;
                if (provider.getClass().equals(existing.getClass()))
                    continue mainLoop;
            }

            toAdd.add(provider);
        }

        DatabaseManager.providers.addAll(toAdd);
    }

    /**
     * Create the logger manager
     */
    private DatabaseManager() {
        load();
    }

    /**
     * Update the logger manager providers
     *
     * @return the logger manager
     */
    public DatabaseManager update() {
        load();
        return this;
    }

    /**
     * Get the log provider
     *
     * @return the provider
     */
    public DatabaseProvider getProvider() {
        return getProvider(DatabaseProvider.class);
    }

    /**
     * Get the log provider
     *
     * @param type the provider type
     * @return the provider
     * @param <T> the provider type
     * @throws NullPointerException if the type is null
     * @throws IllegalStateException if there's no provider
     */
    public <T extends DatabaseProvider> T getProvider(final Class<T> type) throws NullPointerException, IllegalStateException {
        if (type == null) throw new NullPointerException();

        for (DatabaseProvider provider : DatabaseManager.providers) {
            if (provider == null) continue;

            Class<?> providerType = provider.getClass();
            if (type.isAssignableFrom(providerType))
                return type.cast(provider);
        }

        throw new IllegalStateException("Could not find database engine for " + type.getSimpleName());
    }

    /**
     * Get the manager instance
     *
     * @return the manager instance
     */
    public static DatabaseManager getInstance() {
        return DatabaseManager.INSTANCE;
    }
}