package es.karmadev.api.database;

import es.karmadev.api.database.json.DatabaseEngine;

import java.nio.file.Path;

/**
 * Represents a database provider
 */
public interface DatabaseProvider {

    /**
     * Get the default engine at the
     * default working directory
     *
     * @return the default engine
     */
    DatabaseEngine getEngine();

    /**
     * Get the database engine
     *
     * @param workingDirectory the engine working
     *                         directory
     * @return the engine
     */
    DatabaseEngine getEngine(final Path workingDirectory);
}
