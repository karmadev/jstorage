package es.karmadev.api.database.json.provider;

import es.karmadev.api.database.json.DatabaseConnection;
import es.karmadev.api.database.json.DatabaseEngine;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Json database model
 */
@SuppressWarnings("unused")
public final class JsonDatabase implements DatabaseEngine {

    private final Map<Path, JsonConnection> connections = new ConcurrentHashMap<>();
    private final Path databaseLocation;

    /**
     * Create a new database
     *
     * @param databaseLocation the database location
     */
    public JsonDatabase(final Path databaseLocation) {
        this.databaseLocation = databaseLocation;
    }

    /**
     * Get if the engine is protected
     *
     * @return if the engine is protected
     */
    @Override
    public boolean isProtected() {
        return true;
    }

    /**
     * Get the engine name
     *
     * @return the engine name
     */
    @Override
    public String getName() {
        return "json";
    }

    /**
     * Grab a connection from the engine
     * connection pool (if any)
     *
     * @param name the connection name
     * @return a database connection
     */
    @Override
    public DatabaseConnection grabConnection(final String name) {
        Path file = this.databaseLocation;

        if (name.contains(File.pathSeparator)) {
            String[] data = name.split(File.pathSeparator);
            int index = 0;
            for (String str : data) {
                if (index + 1 == data.length) {
                    if (!str.endsWith(".json")) {
                        str += ".json";
                    }
                }

                file = file.resolve(str);
            }
        } else {
            if (name.endsWith(".json")) {
                file = file.resolve(name);
            } else {
                file = file.resolve(name + ".json");
            }
        }

        Path targetFile = file;
        return connections.computeIfAbsent(targetFile, (e) ->
                new JsonConnection(targetFile, null, null));
    }
}
