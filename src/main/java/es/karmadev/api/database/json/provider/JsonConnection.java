package es.karmadev.api.database.json.provider;

import es.karmadev.api.database.json.DatabaseConnection;
import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.api.kson.io.JsonWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * KarmaAPI json connection
 */
@SuppressWarnings("unused")
public class JsonConnection implements DatabaseConnection {

    final Path file;
    final String table;
    private final JsonConnection parent;

    JsonObject database;

    boolean pretty = false;
    boolean autoSave = false;

    public JsonConnection(final Path file, final JsonConnection parent, final String table) {
        this.file = file;
        this.parent = parent;
        this.table = table;

        String raw = "";
        if (Files.exists(file)) {
            try {
                raw = new String(
                        Files.readAllBytes(file)
                );
            } catch (IOException ex) {
                RuntimeException rEx = new RuntimeException();
                rEx.addSuppressed(ex);

                throw rEx;
            }
        }

        String path = "";
        if (parent != null) {
            String parentPath = parent.database.getPath();
            if (parentPath.isEmpty()) {
                path = parent.database.getKey();
            } else {
                path = parentPath + parent.database.getPathSeparator() + parent.database.getKey();
            }
        }

        JsonObject tmpObject = JsonObject.newObject(path, table);
        if (raw.startsWith("{") && raw.endsWith("}")) {
            tmpObject = JsonReader.read(raw).asObject();
        }

        database = tmpObject;
        if (!database.hasChild("types") || !database.getChild("types").isObjectType()) {
            JsonObject typesObject = JsonObject.newObject((parent == null ? "" : parent.database.getPath()), "types");
            typesObject.put("schemed", false);
            database.put("types", typesObject);
            save();
        }
    }

    /**
     * Get the table name
     *
     * @return the table name
     */
    @Override
    public String getTable() {
        return this.table;
    }

    /**
     * Get the parent connection
     *
     * @return the parent connection
     */
    @Override
    public JsonConnection getParent() {
        return this.parent;
    }

    /**
     * Set the pretty save status
     *
     * @param status if the json database
     *               stores in pretty format
     */
    @Override
    public void setPrettySave(final boolean status) {
        if (parent != null) {
            parent.setPrettySave(status);
            return;
        }

        pretty = status;
    }

    /**
     * Get if the database has the specified table
     *
     * @param name the table name
     * @return if the database has the table
     */
    @Override
    public boolean hasTable(final String name) {
        return database.hasChild(name);
    }

    /**
     * Remove a table
     *
     * @param name the table name
     */
    @Override
    public void removeTable(final String name) {
        if (database.hasChild(table)) {
            JsonInstance element = database.getChild(name);
            if (element.isObjectType()) database.removeChild(name);

            JsonObject typesObject = database.getChild("types").asObject();
            typesObject.removeChild(name);
        }
    }

    /**
     * Create a table
     *
     * @param name the table name
     * @return the child connection for the table
     * @throws UnsupportedOperationException if the table name is already taken by a non-table object
     */
    @Override
    public JsonConnection createTable(final String name) throws UnsupportedOperationException {
        JsonObject typesObject = database.getChild("types").asObject();
        JsonObject child = JsonObject.newObject(database.getPath(), name);
        if (database.hasChild(name)) {
            JsonInstance element = database.getChild(name);
            if (!element.isObjectType()) throw new UnsupportedOperationException("Cannot create a table " + name + " because another field with that name already exists");

            if (typesObject.hasChild(name)) {
                String type = typesObject.getChild(name).asString();
                if (type != null && !type.equals("table")) throw new UnsupportedOperationException("Cannot create a table " + name + " because another field with that name already exists");
            }

            child = element.asObject();
        }

        if (!child.hasChild("types")) {
            JsonObject types = JsonObject.newObject(database.getPath(), "types");
            types.put("schemed", false);
            child.put("types", types);
        }

        database.put(name, child);
        JsonConnection connection = new JsonConnection(file, this, name);
        connection.database = child;

        typesObject.put(name, "table");

        return connection;
    }

    /**
     * Create a list of tables
     *
     * @param database the table database name
     * @param names the table names
     * @return the child connection for the table
     * @throws UnsupportedOperationException if the database name is already taken by another non-database object
     */
    @Override
    public List<JsonConnection> createTables(final String database, final String... names) throws UnsupportedOperationException {
        JsonObject typesObject = this.database.getChild("types").asObject();
        JsonArray array = JsonArray.newArray(this.database.getPath(), database);

        if (this.database.hasChild(database)) {
            JsonInstance element = this.database.getChild(database);
            if (!element.isArrayType()) throw new UnsupportedOperationException("Cannot redefine field " + database + " because existing type is not a database");

            array = element.asArray();
        }

        boolean check = !array.isEmpty();
        Map<String, JsonObject> map = new HashMap<>();
        if (check) {
            for (JsonInstance element : array) {
                if (!element.isObjectType()) throw new UnsupportedOperationException("Cannot add object to non-object list!");

                JsonObject object = element.asObject();
                if (!object.hasChild("name")) throw new UnsupportedOperationException("Cannot add table to non-table list!");

                map.put(object.getChild("name").asString(), element.asObject());
            }
        }

        List<JsonConnection> connections = new ArrayList<>();
        for (String name : names) {
            JsonObject child = JsonObject.newObject(this.database.getPath(), name);
            if (map.containsKey(name)) child = map.get(name);

            if (!child.hasChild("types")) {
                JsonObject types = JsonObject.newObject(this.database.getPath() + '.' + name, "types");
                types.put("schemed", false);
                child.put("types", types);
            }
            array.add(child);

            JsonConnection connection = new JsonConnection(file, this, name);
            connection.database = child;

            typesObject.put(name, "table");
            connections.add(connection);
        }
        this.database.put(database, array);

        return connections;
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-primitive
     * object
     */
    @Override
    public void set(final String key, final Map<String, Object> value) throws UnsupportedOperationException {
        JsonObject typesObject = this.database.getChild("types").asObject();
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isObjectType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");
        }

        if (value != null) {
            JsonObject element = JsonReader.readTree(value);
            database.put(key, element);
            typesObject.put(key, "map");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-primitive object
     */
    @Override
    public void set(final String key, final String value) throws UnsupportedOperationException {
        JsonObject typesObject = this.database.getChild("types").asObject();
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isNativeType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            JsonNative primitive = element.asNative();
            if (!primitive.isString()) throw new UnsupportedOperationException("Cannot set string to non-string field!");
        }

        if (value != null) {
            database.put(key, value);
            typesObject.put(key, "string");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-primitive object
     */
    @Override
    public void set(final String key, final Number value) throws UnsupportedOperationException {
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isNativeType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            JsonNative primitive = element.asNative();
            if (!primitive.isNumber()) throw new UnsupportedOperationException("Cannot set number to non-number field!");

            if (value != null) {
                String currentType = getType(key);
                String typeName = getTypeName(value);

                JsonObject typesObject = database.getChild("types").asObject();
                if (!typeName.equalsIgnoreCase(currentType)) {
                    throw new UnsupportedOperationException("Cannot set number of " + key + " to non-" + typeName + " value!");
                }
            }
        }

        if (value != null) {
            database.put(key, value);

            JsonObject typesObject = database.getChild("types").asObject();
            String typeName = getTypeName(value);
            typesObject.put(key, typeName);
        } else {
            database.removeChild(key);

            JsonObject typesObject = database.getChild("types").asObject();
            typesObject.removeChild(key);
        }
    }

    private String getTypeName(final Number value) {
        if (value instanceof Byte) {
            return "byte";
        }
        if (value instanceof Short) {
            return "short";
        }
        if (value instanceof Integer) {
            return "integer";
        }
        if (value instanceof Long) {
            return "long";
        }
        if (value instanceof Float) {
            return "float";
        }

        return "double";
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-primitive object
     */
    @Override
    public void set(final String key, final Boolean value) throws UnsupportedOperationException {
        JsonObject typesObject = database.getChild("types").asObject();
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isNativeType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            JsonNative primitive = element.asNative();
            if (!primitive.isBoolean()) throw new UnsupportedOperationException("Cannot set boolean to non-boolean field!");
        }

        if (value != null) {
            database.put(key, value);

            typesObject.put(key, "boolean");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    @Override
    public void setStringList(final String key, final List<String> value) throws UnsupportedOperationException {
        JsonObject typesObject = database.getChild("types").asObject();
        JsonArray array = JsonArray.newArray(this.database.getPath(), key);
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isArrayType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            array = element.asArray();
        }

        boolean check = !array.isEmpty();
        if (check) {
            JsonInstance firstElement = array.get(0);
            if (!firstElement.isNativeType()) throw new UnsupportedOperationException("Cannot add primitive to non-primitive list!");

            JsonNative primitive = firstElement.asNative();
            if (!primitive.isString()) throw new UnsupportedOperationException("Cannot add string to non-string list!");
        }

        if (value != null) {
            for (String s : value) array.add(s);
            database.put(key, array);

            typesObject.put(key, "stringList");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    @Override
    public void setNumberList(final String key, final List<Number> value) throws UnsupportedOperationException {
        JsonObject typesObject = database.getChild("types").asObject();
        JsonArray array = JsonArray.newArray(this.database.getPath(), key);
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isArrayType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            array = element.asArray();
        }

        boolean check = !array.isEmpty();
        if (check) {
            JsonInstance firstElement = array.get(0);
            if (!firstElement.isNativeType()) throw new UnsupportedOperationException("Cannot add primitive to non-primitive list!");

            JsonNative primitive = firstElement.asNative();
            if (!primitive.isNumber()) throw new UnsupportedOperationException("Cannot add number to non-number list!");
        }

        if (value != null) {
            for (Number n : value) array.add(n);
            database.put(key, array);

            typesObject.put(key, "numberList");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    @Override
    public void setBooleanList(final String key, final List<Boolean> value) throws UnsupportedOperationException {
        JsonObject typesObject = database.getChild("types").asObject();
        JsonArray array = JsonArray.newArray(this.database.getPath(), key);
        if (database.hasChild(key)) {
            JsonInstance element = database.getChild(key);
            if (!element.isArrayType()) throw new UnsupportedOperationException("Cannot redefine field " + key + " because existing type doesn't match new type");

            array = element.asArray();
        }

        boolean check = !array.isEmpty();
        if (check) {
            JsonInstance firstElement = array.get(0);
            if (!firstElement.isNativeType()) throw new UnsupportedOperationException("Cannot add primitive to non-primitive list!");

            JsonNative primitive = firstElement.asNative();
            if (!primitive.isBoolean()) throw new UnsupportedOperationException("Cannot add boolean to non-boolean list!");
        }

        if (value != null) {
            for (boolean b : value) array.add(b);
            database.put(key, array);

            typesObject.put(key, "booleanList");
        } else {
            database.removeChild(key);
            typesObject.removeChild(key);
        }
    }

    /**
     * Get a map
     *
     * @param key the map key
     * @return the map
     */
    @Override
    public Map<String, Object> getMap(final String key) {
        if (!database.hasChild(key)) return null;
        JsonInstance element = database.getChild(key);

        if (getType(key).equals("map")) {
            return element.getTree();
        }

        return null;
    }

    /**
     * Get a string
     *
     * @param key the string key
     * @param def the default value
     * @return the string
     */
    @Override
    public String getString(final String key, final String def) {
        JsonNative primitive = getPrimitive(key);
        if (primitive == null) return def;

        if (primitive.isString()) return primitive.getAsString();
        return def;
    }

    /**
     * Get a number
     *
     * @param key the number key
     * @return the number
     */
    @Override
    public Number getNumber(final String key, final Number def) {
        JsonNative primitive = getPrimitive(key);
        if (primitive == null) return def;

        if (primitive.isNumber()) return primitive.getAsNumber();
        return def;
    }

    /**
     * Get a boolean
     *
     * @param key the boolean key
     * @param def the default value
     * @return the boolean
     */
    @Override
    public Boolean getBoolean(final String key, final Boolean def) {
        JsonNative primitive = getPrimitive(key);
        if (primitive == null) return def;

        if (primitive.isBoolean()) return primitive.asBoolean();
        return def;
    }

    /**
     * Get a list of strings
     *
     * @param key the list key
     * @param def the default value
     * @return the list
     */
    @Override
    public List<String> getStringList(final String key, final Collection<String> def) {
        if (!database.hasChild(key)) return new ArrayList<>(def);
        JsonInstance element = database.getChild(key);

        if (!element.isArrayType()) return new ArrayList<>(def);
        JsonArray array = element.asArray();

        List<String> strings = new ArrayList<>();
        for (JsonInstance child : array) {
            if (!child.isNativeType()) continue;
            JsonNative primitive = child.asNative();

            if (!primitive.isString()) continue;
            strings.add(primitive.getAsString());
        }

        return strings;
    }

    /**
     * Get a list of numbers
     *
     * @param key the list key
     * @param def the default value
     * @return the list
     */
    @Override
    public List<Number> getNumberList(final String key, final Collection<Number> def) {
        if (!database.hasChild(key)) return new ArrayList<>(def);
        JsonInstance element = database.getChild(key);

        if (!element.isArrayType()) return new ArrayList<>(def);
        JsonArray array = element.asArray();

        List<Number> numbers = new ArrayList<>();
        for (JsonInstance child : array) {
            if (!child.isNativeType()) continue;
            JsonNative primitive = child.asNative();

            if (!primitive.isNumber()) continue;
            numbers.add(primitive.getAsNumber());
        }

        return numbers;
    }

    /**
     * Get a list of booleans
     *
     * @param key the list key
     * @param def the default value
     * @return the list
     */
    public List<Boolean> getBooleanList(final String key, final Collection<Boolean> def) {
        if (!database.hasChild(key)) return new ArrayList<>(def);
        JsonInstance element = database.getChild(key);

        if (!element.isArrayType()) return new ArrayList<>(def);
        JsonArray array = element.asArray();

        List<Boolean> booleans = new ArrayList<>();
        for (JsonInstance child : array) {
            if (!child.isNativeType()) continue;
            JsonNative primitive = child.asNative();

            if (!primitive.isBoolean()) continue;
            booleans.add(primitive.getAsBoolean());
        }

        return booleans;
    }

    /**
     * Get a list of tables
     *
     * @param key the list key
     * @return the list
     */
    @Override
    public List<JsonConnection> getTableList(final String key) {
        if (!database.hasChild(key)) return null;
        JsonInstance element = database.getChild(key);

        if (!element.isArrayType()) return null;
        JsonArray array = element.asArray();

        List<JsonConnection> tables = new ArrayList<>();
        for (JsonInstance child : array) {
            if (!child.isObjectType()) continue;
            JsonObject object = child.asObject();

            if (!object.hasChild("name")) continue;
            String tableName = object.getChild("name").asString();

            JsonConnection connection = new JsonConnection(file, this, tableName);
            connection.database = object;

            tables.add(connection);
        }

        return tables;
    }

    /**
     * Get the type of the field key
     *
     * @param key the key
     * @return the key type
     */
    @Override
    public String getType(final String key) {
        JsonObject typesObject = database.getChild("types").asObject();
        if (typesObject.hasChild(key) && typesObject.getChild(key).isNativeType()) {
            return typesObject.getChild(key).asString();
        }

        return "null";
    }

    /**
     * Get if the key is set
     *
     * @param key the key
     * @return if the key is set
     */
    @Override
    public boolean isSet(final String key) {
        return database.hasChild(key);
    }

    /**
     * Unset a key
     *
     * @param key the key to unset
     * @throws UnsupportedOperationException if the unset operation is not
     *                                       supported for the value of the key
     */
    @Override
    public void unset(final String key) throws UnsupportedOperationException {
        if (isSet(key)) {
            String type = getType(key);
            if (type.equals("table"))
                throw new UnsupportedOperationException("Cannot unset table " + key + " with unset method. Use removeTable method instead");

            this.database.removeChild(key);
        }
    }

    /**
     * Get all the database keys
     *
     * @return the database keys
     */
    @Override
    public Collection<String> getKeys() {
        JsonObject typesObject = database.getChild("types").asObject();
        return typesObject.getKeys(true);
    }

    /**
     * Save all changes into the local database
     *
     * @return the save task
     */
    @Override
    public CompletableFuture<Boolean> save() {
        if (parent == null) {
            return CompletableFuture.supplyAsync(() -> {
                String raw = database.toString(pretty);
                JsonWriter writer = new JsonWriter(database);
                writer.setPrettyPrinting(true);
                try {
                    Files.write(file, new byte[0]);
                    writer.export(Files.newBufferedWriter(file,
                            StandardOpenOption.CREATE));

                    return true;
                } catch (IOException ex) {
                    RuntimeException rEx = new RuntimeException();
                    rEx.addSuppressed(ex);

                    throw rEx;
                }
            });
        }

        return parent.save();
    }

    private JsonNative getPrimitive(final String key) {
        if (!database.hasChild(key)) return null;
        JsonInstance element = database.getChild(key);

        if (!element.isNativeType()) return null;
        return element.asNative();
    }
}
