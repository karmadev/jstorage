package es.karmadev.api.database.json.provider;

import es.karmadev.api.database.DatabaseProvider;
import es.karmadev.api.database.json.DatabaseEngine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default database provider
 */
public class DefaultProvider implements DatabaseProvider {

    private final Map<Path, DatabaseEngine> engines = new ConcurrentHashMap<>();

    /**
     * Get the default engine at the
     * default working directory
     *
     * @return the default engine
     */
    @Override
    public DatabaseEngine getEngine() {
        return getEngine(Paths.get("databases"));
    }

    /**
     * Get the database engine
     *
     * @param workingDirectory the engine working
     *                         directory
     * @return the engine
     */
    @Override
    public DatabaseEngine getEngine(final Path workingDirectory) {
        return engines.computeIfAbsent(workingDirectory, (e) -> new JsonDatabase(workingDirectory));
    }
}
