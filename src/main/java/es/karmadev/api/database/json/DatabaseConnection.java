package es.karmadev.api.database.json;

import es.karmadev.api.database.json.provider.JsonConnection;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * KarmaAPI database connection
 */
public interface DatabaseConnection {

    /**
     * Get the table name
     *
     * @return the table name
     */
    String getTable();

    /**
     * Get the parent connection
     *
     * @return the parent connection
     */
    DatabaseConnection getParent();

    /**
     * Set the pretty save status
     *
     * @param status if the json database
     *               stores in pretty format
     */
    void setPrettySave(final boolean status);

    /**
     * Get if the database has the specified table
     *
     * @param name the table name
     * @return if the database has the table
     */
    boolean hasTable(final String name);

    /**
     * Remove a table
     *
     * @param name the table name
     */
    void removeTable(final String name);

    /**
     * Create a table
     *
     * @param name the table name
     * @return the child connection for the table
     * @throws UnsupportedOperationException if the table name is already taken by a non-table object
     */
    JsonConnection createTable(final String name) throws UnsupportedOperationException;

    /**
     * Create a list of tables
     *
     * @param database the table database name
     * @param names the table names
     * @return the child connection for the table
     * @throws UnsupportedOperationException if the database name is already taken by another non-database object
     */
    List<JsonConnection> createTables(final String database, final String... names) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void set(final String key, final Map<String, Object> value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void set(final String key, final String value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void set(final String key, final Number value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void set(final String key, final Boolean value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void setStringList(final String key, final List<String> value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void setNumberList(final String key, final List<Number> value) throws UnsupportedOperationException;

    /**
     * Set a value
     *
     * @param key the value key
     * @param value the value
     * @throws UnsupportedOperationException if the field is already occupied by another non-list object
     */
    void setBooleanList(final String key, final List<Boolean> value) throws UnsupportedOperationException;

    /**
     * Get a map
     *
     * @param key the map key
     * @return the map
     */
    Map<String, Object> getMap(final String key);

    /**
     * Get a string
     *
     * @param key the string key
     * @param def the default value
     * @return the string
     */
    String getString(final String key, final String def);

    /**
     * Get a string
     *
     * @param key the string key
     * @return the string
     */
    default String getString(final String key) {
        return getString(key, null);
    }

    /**
     * Get a number
     *
     * @param key the number key
     * @param def the default value
     * @return the number
     */
    Number getNumber(final String key, final Number def);

    /**
     * Get a number
     *
     * @param key the number key
     * @return the number
     */
    default Number getNumber(final String key) {
        return getNumber(key, null);
    }

    /**
     * Get a boolean
     *
     * @param key the boolean key
     * @param def the default value
     * @return the boolean
     */
    Boolean getBoolean(final String key, final Boolean def);

    /**
     * Get a boolean
     *
     * @param key the boolean key
     * @return the boolean
     */
    default Boolean getBoolean(final String key) {
        return getBoolean(key, null);
    }

    /**
     * Get a list of strings
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    List<String> getStringList(final String key, final Collection<String> def);

    /**
     * Get a list of strings
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    default List<String> getStringList(final String key, final String... def) {
        return getStringList(key, Arrays.asList(def));
    }

    /**
     * Get a list of strings
     *
     * @param key the list key
     * @return the list
     */
    default List<String> getStringList(final String key) {
        return getStringList(key, (Collection<String>) null);
    }

    /**
     * Get a list of numbers
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    List<Number> getNumberList(final String key, final Collection<Number> def);

    /**
     * Get a list of numbers
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    default List<Number> getNumberList(final String key, final Number... def) {
        return getNumberList(key, Arrays.asList(def));
    }

    /**
     * Get a list of numbers
     *
     * @param key the list key
     * @return the list
     */
    default List<Number> getNumberList(final String key) {
        return getNumberList(key, (Collection<Number>) null);
    }

    /**
     * Get a list of booleans
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    List<Boolean> getBooleanList(final String key, final Collection<Boolean> def);

    /**
     * Get a list of booleans
     *
     * @param key the list key
     * @param def the default values
     * @return the list
     */
    default List<Boolean> getBooleanList(final String key, final Boolean... def) {
        return getBooleanList(key, Arrays.asList(def));
    }

    /**
     * Get a list of booleans
     *
     * @param key the list key
     * @return the list
     */
    default List<Boolean> getBooleanList(final String key) {
        return getBooleanList(key, (Collection<Boolean>) null);
    }

    /**
     * Get a list of tables
     *
     * @param key the list key
     * @return the list
     */
    List<JsonConnection> getTableList(final String key);

    /**
     * Get the type of the field key
     *
     * @param key the key
     * @return the key type
     */
    String getType(final String key);

    /**
     * Get if the key is set
     *
     * @param key the key
     * @return if the key is set
     */
    boolean isSet(final String key);

    /**
     * Unset a key
     *
     * @param key the key to unset
     * @throws UnsupportedOperationException if the unset operation is not
     * supported for the value of the key
     */
    void unset(final String key) throws UnsupportedOperationException;

    /**
     * Get all the database keys
     *
     * @return the database keys
     */
    Collection<String> getKeys();

    /**
     * Save all changes into the local database
     *
     * @return the save task
     */
    CompletableFuture<Boolean> save();

}
