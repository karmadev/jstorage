<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>es.karmadev.api</groupId>
	<artifactId>JStorage</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<packaging>jar</packaging>

	<description>JStorage is a json database system using OOP instead of commands</description>

	<licenses>
		<license>
			<name>The MIT License</name>
			<url>https://license.karmadev.es/-2025</url>
			<distribution>repo</distribution>
			<comments>
				The MIT License (MIT)
				Copyright © 2024-2025 KarmaDev

				Permission is hereby granted, free of charge, to any person obtaining a copy of this software
				and associated documentation files (the “Software”), to deal in the Software without
				restriction, including without limitation the rights to use, copy, modify, merge, publish,
				distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
				Software is furnished to do so, subject to the following conditions:

				The above copyright notice and this permission notice shall be included in all copies or
				substantial portions of the Software.

				THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
				BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
				NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
				DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
				FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
			</comments>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:https://gitlab.com/karmadev/jstorage.git</connection>
		<developerConnection>scm:git:https://gitlab.com/karmadev/jstorage.git</developerConnection>
		<url>https://gitlab.com/karmadev/jstorage</url>
		<tag>HEAD</tag>
	</scm>

	<developers>
		<developer>
			<id>karma</id>
			<name>KarmaDev</name>
			<email>karmadev.es@gmail.com</email>
			<organization>RedDo</organization>
			<organizationUrl>https://reddo.es</organizationUrl>
			<timezone>UTC+2</timezone>
			<roles>
				<role>CEO</role>
				<role>Developer</role>
			</roles>
		</developer>
	</developers>

	<properties>
		<maven.compiler.source>8</maven.compiler.source>
		<maven.compiler.target>8</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>2.2.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<additionalOptions>
						<additionalOption>-Xdoclint:none</additionalOption>
					</additionalOptions>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>karmadev_releases</id>
			<url>https://nexus.karmadev.es/repository/internal/</url>
		</repository>
		<repository>
			<id>karmadev_snapshots</id>
			<url>https://nexus.karmadev.es/repository/snapshots/</url>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>5.11.0-M1</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>es.karmadev</groupId>
			<artifactId>KSon</artifactId>
			<version>1.0.6-SNAPSHOT</version>
			<scope>compile</scope>
		</dependency>
	</dependencies>

	<distributionManagement>
		<repository>
			<id>karmadev_releases</id>
			<url>${TARGET_RELEASES}</url>
		</repository>
		<snapshotRepository>
			<id>karmadev_snapshots</id>
			<url>${TARGET_SNAPSHOTS}</url>
		</snapshotRepository>
	</distributionManagement>
</project>